const AWS = require('aws-sdk');
var Validator = require('jsonschema').Validator;

const dynamodb = new AWS.DynamoDB.DocumentClient({
  apiVersion: '2012-08-10',
  endpoint: new AWS.Endpoint('http://localhost:8000'),
  region: 'us-west-2',
  accessKeyId: 'fakeKeyId',
  secretAccessKey: 'fakeSecretAccessKey'
  // what could you do to improve performance?
});

const tableName = 'SchoolStudents';

/**
 * The entry point into the lambda
 *
 * @param {Object} event
 * @param {string} event.schoolId
 * @param {string} event.studentId
 */
exports.handler = async (event) => {
  var params = {
    TableName : tableName,
    Key: {
        schoolId: event.schoolId,
        studentId: event.studentId
    }
  };
  
  return await dynamodb.delete(params).promise();
};